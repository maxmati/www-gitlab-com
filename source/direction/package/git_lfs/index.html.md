---
layout: markdown_page
title: "Category Direction - Git LFS"
---

- TOC
{:toc}

## Git LFS

Git LFS (Large File Storage) is a Git extension, which reduces the impact of large files in your repository by downloading the relevant versions of them lazily. Specifically, large files are downloaded during the checkout process rather than during cloning or fetching..

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=lfs)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)
- [Documentation](https://docs.gitlab.com/ee/administration/lfs/manage_large_binaries_with_git_lfs.html)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## Use cases

1. Version large files—even those as large as a couple GB in size—with Git.
1. Automatically detect LFS-tracked files and clone them via HTTP
1. Download less data. This means faster cloning and fetching from repositories that deal with large files.
1. Host more in your Git repositories. External file storage makes it easy to keep your repository at a manageable size.

## What's next & why
TBD

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).

Key deliverables to achieve this are:
- TBD

## Competitive landscape

* [Atlassian](https://www.atlassian.com/git/tutorials/git-lfs)
* [GitHub](https://github.com/)
* [JFrog](https://jfrog.com/) 

​​## Top Customer Success/Sales issue(s)
TBD

## Top user issue(s)
TBD

## Top internal customer issue(s)
TBD

## Top Vision Item(s)
TBD
