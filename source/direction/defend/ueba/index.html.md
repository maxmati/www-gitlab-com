---
layout: markdown_page
title: "Category Direction - User Entity and Behavioral Analytics"
---

## Description
User Entity and Behavior Analytics (UEBA) is a way to identify attacks and high risk behaviors by correlating different data sources and observing behavioral patterns. This allows attacks to be observed that are only apparent with context from multiple sources, rather than just a single event in isolation.

### Goal

GitLab's goal is to provide UEBA capabilities for applications to help proactively identify malicious traffic, potentially compromised user accounts, and high-risk behaviors, so that actionable remediation steps are possible. 

Our UEBA goals align with the goals of our [Security Paradigm](https://about.gitlab.com/direction/secure/#security-paradigm) in that we strive to offer these capabilities "batteries included" with minimal to no configuration for initial usage. We also strive to provide UEBA results in an actionable, prioritized format for triage and remediation. We will not block by default unless explicitly configured to do so.

Another UEBA goal is that GitLab will feedback our results to other stages so that any necessary action can be taken there, in addition to the defending the app itself. An example could include creating an issue about access controls for a specific region of the app that is being exploited by malicious users.

### Roadmap
TODO

## What's Next & Why
TODO

## Competitive Landscape
eSecurity Planet has a [UEBA buying guide](https://www.esecurityplanet.com/products/top-ueba-vendors.html) which has a list of competitive offerings.

## Analyst Landscape
TODO

## Top Customer Success/Sales Issue(s)

There is no feature available for this category.

## Top Customer Issue(s)

The category is very new, so we still need to engage customers and get feedback about their interests and priorities in this area.

## Top Vision Item(s)
TODO
